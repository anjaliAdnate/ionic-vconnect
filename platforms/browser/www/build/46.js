webpackJsonp([46],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadDocPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, actionSheetCtrl, camera, file, platform, filePath, toastCtrl, loadingCtrl, transfer, transferObj, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.file = file;
        this.platform = platform;
        this.filePath = filePath;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.transfer = transfer;
        this.transferObj = transferObj;
        this.apiCall = apiCall;
        this.lastImage = null;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        console.log("islogin devices => " + this.vehData.imageDoc);
        console.log("islogin devices => " + this.vehData.imageDoc.length);
        if (this.vehData.imageDoc.length > 0) {
            console.log("imagedoc data: " + JSON.stringify(this.vehData.imageDoc));
            for (var i = 0; i < this.vehData.imageDoc.length; i++) {
                if (this.vehData.imageDoc[i].doctype == 'RC')
                    this.rcPresent = true;
                if (this.vehData.imageDoc[i].doctype == 'PUC')
                    this.pucPresent = true;
                if (this.vehData.imageDoc[i].doctype == 'Insurance')
                    this.insurencePresent = true;
                if (this.vehData.imageDoc[i].doctype == 'Licence')
                    this.licencePresent = true;
            }
        }
    }
    UploadDocPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.firstFunc = function (type) {
        this.myVar = type;
        var that = this;
        if (type == 'RC') {
            if (!that.rcPresent)
                this.docString = type;
            else
                this.showToast('RC doc already uploaded!');
        }
        else {
            if (type == 'PUC') {
                if (!that.pucPresent)
                    this.docString = type;
                else
                    this.showToast('PUC doc already uploaded!');
            }
            else {
                if (type == 'Insurance') {
                    if (!that.insurencePresent)
                        this.docString = type;
                    else
                        this.showToast('Insurance doc already uploaded!');
                }
                else {
                    if (type == 'Licence') {
                        if (!that.licencePresent)
                            this.docString = type;
                        else
                            this.showToast('Licence doc already uploaded!');
                    }
                }
            }
        }
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    UploadDocPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            // this.presentToast('Error while selecting image.');
            console.log("Error while selecting image.", err);
        });
    };
    UploadDocPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    UploadDocPage.prototype.pathForImage = function (img) {
        console.log("Image=>", img);
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    UploadDocPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    UploadDocPage.prototype.uploadImage = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/users/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(function (data) {
            _this.Imgloading.dismissAll();
            _this.dlUpdate(data.response);
        }, function (err) {
            console.log("uploadError=>", err);
            _this.lastImage = null;
            _this.Imgloading.dismissAll();
            _this.presentToast('Error while uploading file, Please try again !!!');
        });
    };
    UploadDocPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    UploadDocPage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        var that = this;
        var imageDoc = {
            "doctype": that.docString,
            "image": dllink,
            "phone": that.islogin.phn,
            "docdate": new Date(that.docexp_date).toISOString(),
            "docname": that.docname
        };
        var dlObj = {
            "_id": that.vehData._id,
            "imageDoc": imageDoc
        };
        this.apiCall.startLoading();
        this.apiCall.deviceupdateCall(dlObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            debugger;
            _this.presentToast('Image succesful uploaded.');
            console.log("returned obj: ", res);
            that.lastImage = null;
            that.docImge = undefined;
            that.docString = undefined;
            that.docname = undefined;
            that.docexp_date = undefined;
            _this.checkDevice();
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.presentToast('Internal server Error !!!');
        });
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("single data: " + JSON.stringify(data));
            console.log("single data: " + data);
            if (data.imageDoc.length > 0) {
                console.log("imagedoc data: " + JSON.stringify(data.imageDoc));
                for (var i = 0; i < data.imageDoc.length; i++) {
                    if (data.imageDoc[i].doctype == 'RC')
                        _this.rcPresent = true;
                    if (data.imageDoc[i].doctype == 'PUC')
                        _this.pucPresent = true;
                    if (data.imageDoc[i].doctype == 'Insurance')
                        _this.insurencePresent = true;
                    if (data.imageDoc[i].doctype == 'Licence')
                        _this.licencePresent = true;
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"/Users/admin/Library/Mobile Documents/com~apple~CloudDocs/Desktop/IONIC_PROJECTS/IONIC_APPS/vconnect/src/pages/add-devices/upload-doc/upload-doc.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Upload Document</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-row>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'RC\' && !rcPresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'RC\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">RC</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!rcPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n                      <ion-icon *ngIf="rcPresent" name="cloud-done" color="secondary" style="font-size: 4em"></ion-icon>\n                      <!-- <img src="assets/imgs/right-image-png-4.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                      <!-- <img src="assets/imgs/New Project.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Insurance\' && !insurencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Insurance\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Insurance</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!insurencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n                      </ion-icon>\n                      <ion-icon *ngIf="insurencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                      <!-- <img src="assets/imgs/right-image-png-4.png" height="20" width="20"\n                        style="opacity: 0.5; margin-top: 8px;" /> -->\n                      <!-- <img src="assets/imgs/New Project.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Licence\' && !licencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Licence\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Licence</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!licencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n                      </ion-icon>\n                      <ion-icon *ngIf="licencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                      <!-- <img src="assets/imgs/right-image-png-4.png" height="20" width="20"\n                        style="opacity: 0.5; margin-top: 8px;" /> -->\n                      <!-- <img src="assets/imgs/New Project.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'PUC\' && !licencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'PUC\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">PUC</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!pucPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n                      <ion-icon *ngIf="pucPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                      <!-- <img src="assets/imgs/right-image-png-4.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                      <!-- <img src="assets/imgs/New Project.png" height="50" width="50" style="margin-top: 8px;" /> -->\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <form #docForm="ngForm" *ngIf="docString != undefined" padding>\n    <ion-item>\n      <ion-label floating>{{docString}} Number </ion-label>\n      <ion-input type="text" [(ngModel)]="docname" name="docname"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>Doc Expiry Date </ion-label>\n      <ion-input type="text" onfocus="(this.type=\'date\')" onfocusout="(this.type=\'text\')" [(ngModel)]="docexp_date"\n        name="docexp_date">\n      </ion-input>\n    </ion-item>\n    <button ion-button block color="gpsc" [disabled]="(docname == undefined) || (docexp_date == undefined)"\n      (click)="presentActionSheet()">\n      Select Document\n    </button>\n    <ion-row *ngIf="lastImage !== null">\n      <ion-col>\n        Selected {{docString}} Doc <span style="color: blue;">{{lastImage}}</span>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf="lastImage !== null">\n      <ion-col>\n        <button ion-button block (click)="uploadImage()">Upload Doc</button>\n      </ion-col>\n    </ion-row>\n  </form>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Library/Mobile Documents/com~apple~CloudDocs/Desktop/IONIC_PROJECTS/IONIC_APPS/vconnect/src/pages/add-devices/upload-doc/upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], UploadDocPage);
    return UploadDocPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadDocPageModule", function() { return UploadDocPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__upload_doc__ = __webpack_require__(1028);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UploadDocPageModule = /** @class */ (function () {
    function UploadDocPageModule() {
    }
    UploadDocPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__upload_doc__["a" /* UploadDocPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__upload_doc__["a" /* UploadDocPage */]),
            ],
        })
    ], UploadDocPageModule);
    return UploadDocPageModule;
}());

//# sourceMappingURL=upload-doc.module.js.map

/***/ })

});
//# sourceMappingURL=46.js.map