import { OnInit, Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-report-poi',
    templateUrl: './poi-report.html'
})
export class POIReportPage implements OnInit {
    islogin: any;
    portstemp: any[] = [];
    datetimeStart: any;
    datetimeEnd: any;
    poilist: any[] = [];

    constructor(
        public navCtrl: NavController,
        public navParam: NavParams,
        public apicall: ApiServiceProvider
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = moment({ hours: 0 }).format();
        console.log('start date', this.datetimeStart)
        this.datetimeEnd = moment().format();//new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }

    ngOnInit() {
        this.getdevices();
        this.getpois();
    }

    getpois() {
        this.apicall.getPoisAPI(this.islogin._id)
            .subscribe(data => {
                // console.log("poi list: ", data);
                for (var i = 0; i < data.length; i++) {
                    this.poilist.push(data[i].poi);
                }
                console.log("poi list: ", this.poilist);
                // this.poilist = data;
            },
                err => {
                    console.log("error in pois: ", err)
                })
    }

    getdevices() {
        this.apicall.startLoading().present();
        this.apicall.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(data => {
                this.apicall.stopLoading();
                this.portstemp = data.devices;
            },
                err => {
                    this.apicall.stopLoading();
                    console.log(err);
                });
    }

    getpoiid() {

    }

    getvehicleid() {

    }
}