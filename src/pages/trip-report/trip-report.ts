import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-trip-report',
  templateUrl: 'trip-report.html',
})
export class TripReportPage implements OnInit {
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any = [];
  isdevice: string;
  device_id: any;
  isdeviceTripreport: string;
  datetimeStart: string;
  datetimeEnd: string;
  TripReportData: any[];
  TripsdataAddress: any[];
  deviceId: any;
  distanceBt: number;
  StartTime: string;
  Startetime: string;
  Startdate: string;
  EndTime: string;
  Endtime: string;
  Enddate: string;
  datetime: number;
  did: any;
  vehicleData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if (this.vehicleData == undefined) {
      this.getdevices();
    } else {
      this.device_id = this.vehicleData._id;
      this.getTripReport(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString())
    }
  }

  getdevices() {
    this.apicalligi.startLoading().present();
    this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getTripdevice(item) {
    console.log(item);
    this.device_id = item._id;
    console.log("device id=> " + this.device_id);
    this.did = item.Device_ID;

    localStorage.setItem('devices_id', item);
    this.isdeviceTripreport = localStorage.getItem('devices_id');
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getTripReport(starttime, endtime) {
    this.TripReportData = [];
    if (endtime <= starttime && this.device_id) {
      let alert = this.alertCtrl.create({
        message: 'To time is always greater than From Time',
        buttons: ['OK']
      });
      alert.present();
    } else {
      if (this.device_id == undefined) {
          let alert = this.alertCtrl.create({
            message: 'Please select vehicle first!',
            buttons: ['OK']
          });
          alert.present();
      } else {

        this.apicalligi.startLoading().present();
        this.apicalligi.trip_detailCall(this.islogin._id, new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.device_id)
          .subscribe(data => {

            this.apicalligi.stopLoading();
            if (data.length == 0) {
              let alert = this.alertCtrl.create({
                message: 'No data found',
                buttons: ['OK']
              });
              alert.present();
            } else {
              console.log("response=> " + data);
              this.TripsdataAddress = [];

              for (var i = 0; i < data.length; i++) {

                this.deviceId = data[i]._id;
                this.distanceBt = data[i].distance / 1000;

                this.StartTime = JSON.stringify(data[i].start_time).split('"')[1].split('T')[0];
                var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
                var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
                this.Startetime = gmtDateTime.local().format(' h:mm a');
                this.Startdate = gmtDate.format('ll');

                this.EndTime = JSON.stringify(data[i].end_time).split('"')[1].split('T')[0];
                var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
                var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
                this.Endtime = gmtDateTime1.local().format(' h:mm a');
                this.Enddate = gmtDate1.format('ll');

                var startDate = new Date(data[i].start_time).toLocaleString();
                var endDate = new Date(data[i].end_time).toLocaleString();
             
                var fd = new Date(startDate).getTime();
                var td = new Date(endDate).getTime();
                var time_difference = td - fd;
                var total_min = time_difference / 60000;
                var hours = total_min / 60
                var rhours = Math.floor(hours);
                var minutes = (hours - rhours) * 60;
                var rminutes = Math.round(minutes);
                var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';

                this.TripReportData.push({ 'Startetime': this.Startetime, 'Startdate': this.Startdate, 'Endtime': this.Endtime, 'Enddate': this.Enddate, 'distance': this.distanceBt, '_id': this.deviceId, 'startAddress': data[i].startAddress, 'endAddress': data[i].endAddress, 'start_time': data[i].start_time, 'end_time': data[i].end_time, 'duration': Durations });

              }
            }
          },
            err => {
              this.apicalligi.stopLoading();
              console.log(err);
            });
      }

    }
  }

  tripReview(tripData) {
    this.navCtrl.push('TripReviewPage', {
      params: tripData,
      device_id: this.did
    })
  }

}
